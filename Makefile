# MAKEFILE FILE: Makefile
#
# Purpose   : Build Components Python Mode files.
# Created   : Tuesday, December  8 2020.
# Author    : Pierre Rouleau <prouleau001@gmail.com>
# Time-stamp: <2020-12-08 23:50:17, updated by Pierre Rouleau>
# ----------------------------------------------------------------------------
# Module Description
# ------------------
#
#
#

# ----------------------------------------------------------------------------
# Dependencies
# ------------



# ----------------------------------------------------------------------------
# Portable makefile
.POSIX:

# -----------------------------------------------------------------------------
# allow overriding the Emacs binary at the command line
EMACS = emacs

# Note: the above macro allows the following use of make with
# other Emacs binaries:
#
#    make clean
#    make EMACS=emacs-26.1 pel test
#    make clean
#    make EMACS=emacs-24.3 pel test

# -----------------------------------------------------------------------------
# Define the location of the normal Emacs initialization file.
# This is required for elisp-lint so that it can find the elisp-lint
# and its dependencies.  This can be changed on the command line.
EMACS_INIT = "~/.emacs.d/init.el"

# -----------------------------------------------------------------------------
# Identify the files used in the package.

# The Emacs Lisp files that must be byte-compiled to check their validity.
EL_FILES := pymacs.el \
		python-abbrev-propose.el \
		python-components-auto-fill.el \
		python-components-backward-forms.el \
		python-components-beginning-position-forms.el \
		python-components-booleans-beginning-forms.el \
		python-components-booleans-end-forms.el \
		python-components-close-forms.el \
		python-components-comment.el \
		python-components-copy-forms.el \
		python-components-delete-forms.el \
		python-components-edit.el \
		python-components-electric.el \
		python-components-end-position-forms.el \
		python-components-exec-forms.el \
		python-components-execute-file.el \
		python-components-execute.el \
		python-components-extended-executes.el \
		python-components-extensions.el \
		python-components-extra.el \
		python-components-fast-complete.el \
		python-components-fast-forms.el \
		python-components-ffap.el \
		python-components-foot.el \
		python-components-forms-code.el \
		python-components-forward-forms.el \
		python-components-help.el \
		python-components-hide-show.el \
		python-components-imenu.el \
		python-components-intern.el \
		python-components-kill-forms.el \
		python-components-map.el \
		python-components-mark-forms.el \
		python-components-menu.el \
		python-components-mode.el \
		python-components-move.el \
		python-components-named-shells.el \
		python-components-narrow.el \
		python-components-paragraph.el \
		python-components-pdb.el \
		python-components-pdbtrack.el \
		python-components-section-forms.el \
		python-components-send.el \
		python-components-shell-complete.el \
		python-components-shell-menu.el \
		python-components-shift-forms.el \
		python-components-switches.el \
		python-components-up-down.el \
		python-components-virtualenv.el

ELC_FILES := $(subst .el,.elc,$(EL_FILES))

# ----------------------------------------------------------------------------
# Main target: perform compilation

all: compile

# -----------------------------------------------------------------------------
# Self-descriptive rule: make help prints the info.

.PHONY: help
help:
	@printf "\nComponents Python Mode file maintenance.\n\n"
	@printf "Usage:\n"
	@printf " * make          - Byte-compile all .el files.\n"
	@printf " * make all      - Byte-compile all .el files.\n"
	@printf " * make file.elc - Byte-compile file.el\n"
	@printf " * make help     - Print this help.\n"
	@printf " * make clean    - remove all .elc files in the current directory.\n"

# ----------------------------------------------------------------------------
# Single .el file byte-compile to .elc rule
.SUFFIXES: .el .elc
.el.elc:
	$(EMACS) -Q  --batch -L . -f batch-byte-compile $<

compile: $(ELC_FILES)


# ----------------------------------------------------------------------------
# Remove output files

.PHONY: clean
clean:
	-rm *.elc

# ----------------------------------------------------------------------------
